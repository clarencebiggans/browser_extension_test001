import {formatURL, urlEquality} from "../libs/urls.js";
import {ChromeService} from "../../js/services/chrome.js";
// ---------------------------- controllers/browser.js

// TODO XXX: All chrome.* calls should go to Chrome service and get called within here
// This class should contain the websocket and chrome
// * Contains the chrome stuff to make a better place to call chrome methods
// * Contains the websocket because there should only be 1 websocket contained
//   within a browser session

class BrowserController {
  constructor(chromeService) {
    this.chromeService = chromeService;
  }

  async activeTabId() {
    return new Promise(resolve => {
      this.chromeService.getActiveTabId((result) => {return resolve(result)});
    });
  }

  async getTab(id) {
    return new Promise(resolve => {
      this.chromeService.getTab(id, (result) => {
        if(result === null) {
          console.log("Sending disconnectWebsocket Message, tabId: " + id);
          this.chromeService.sendMessage({
            action: "disconnectWebsocket",
            payload: {err: "Tab Not Found, tabId: " + id},
          });
          // // TODO XXX: REPLICATED CODE
          // if(this.websocketController && this.websocketController.websocket) {
          //   this.websocketController.disconnect();
          // }
        }
        return resolve(result);
      });
    });
  }

  async currentActivatedTabId(id) {
    console.debug('browserController.currentActivatedTabId');

    return new Promise(resolve => {
      this.chromeService.currentActiveTabId((result) => {
        return resolve(result);
      });
    });
  }

  async updateTabUrl(tab, url) {
    console.debug('browserController.updateTabUrl, tab: ' + JSON.stringify(tab) + ', url: ' + JSON.stringify(url))

    return new Promise(resolve => {
      this.chromeService.updateTabUrl(tab.id, formatURL(url), (result) => {
        return resolve(result);
      });
    });
  }

  captureScreenshot(callback) {
    // TODO XXX: Should get the currentWindowID and pass that in to be sure
    //           to capture currect window
    //           * Important for capturing screenshot with voice control
    this.chromeService.captureScreenshot(callback);
  }


  // TODO XXX: should probably just pass in an aciton model object
  async performAction(browserAction) {
    var result;
    var activeTabId = await this.activeTabId();
    var activeTab = await this.getTab(activeTabId);

    if(!activeTab)
      return

    console.log("BrowserController.performAction");
    // Should probably make the type into verb->noun
      // In this case: 'update' -> 'currentURL'
      // Allows to be more flexible and maintain a better naming convention in future ones
    if(browserAction.type === 'updateCurrentURL') {
      console.log('browserAction.payload: ' + JSON.stringify(browserAction.payload));
      var tabUrl = activeTab.url.trim();
      var remoteCurrentUrl = browserAction.payload.url.trim();

      if(!urlEquality(tabUrl, remoteCurrentUrl)) {
        console.log('CHANGING TO REMOTE CURRENT URL')
        this.updateTabUrl(activeTab, remoteCurrentUrl)
      } else {
        console.log('ON CORRECT CURRENT URL, NOT REDIRECTING')
      }
    }

    // Change to updateMediaPlayback
    if(browserAction.type === 'mediaPlayback') {
      console.debug('ACTIVE TAB: ' + JSON.stringify(activeTab));
      // TODO XXX: Chrome Service
      //  * Player
      console.log('sendMessage: ' + JSON.stringify(browserAction));
      chrome.tabs.sendMessage(activeTabId, browserAction.payload);
    }

    console.log("Browser Action: " + JSON.stringify(browserAction));

    return result;
  }
}

export default BrowserController;
