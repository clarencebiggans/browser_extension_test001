import WebSocketLogger from "../loggers/websocket.js";
import {currentTime} from "../libs/time.js";
import {ChromeService} from "../../js/services/chrome.js";
import {MessageV2} from "../../js/models/v2/messages.js";

// ---------------------------- services/websocket.js
/*
NOTE:
* Anything that accesses the Browser/Outside services should be put
  in Services Folder, except repositories that access database-like strucutres
*/

// TODO XXX: Return message instead of action.
//  * Event would get confused on between the different events and the dispatches
class WebSocketMSGLoaders {
  static event(event) {
    var result = {};

    let e = JSON.parse(event.data);
    result['eventName'] = e.eventName;

    let i = e.dynamodb.NewImage;
    if(i.user_id) {
      result['userId'] = i.user_id['S'];
      result['user_id'] = i.user_id['S'];
    }
    if(i.id) {
      result['id'] = i.id['S'];
    }
    if(i.eventType) {
      result['eventType'] = i.eventType['S'];
    }
    if(i.payload) {
      let payload = i.payload['M'];
      var tmp = {};

      for(var key in payload) {
        tmp[key] = payload[key]["S"];
      }
      result['payload'] = tmp;
    }

    let resultEvent = new marks_repo_lib.Event(result);
    console.log('MARKS_REPO_LIB.EVENT: ' + JSON.stringify(resultEvent));
    return resultEvent;
  }

  // TODO XXX: Should figure out way of getting dynamodb object already parsed out to dict instead of this.
  //  * This is more error prone because need to keep track of it
  static message(msg) {
    var result = {};

    let m = JSON.parse(msg.data);
    result['eventName'] = m.eventName;
    
    let i = m.dynamodb.NewImage;
    console.log("WebSocketMSGLoaders.message, after dynamodb.NewImage: " + JSON.stringify(i));
    if(i.user_id) {
      result['userId'] = i.user_id['S'];
      result['user_id'] = i.user_id['S'];
    }
    if(i.id) {
      result['id'] = i.id['S'];
    }
    if(i.eventType) {
      result['actionType'] = i.eventType['S'];
    }
    if(i.payload) {
      let payload = i.payload['M'];
      var tmp = {};

      for(var key in payload) {
        tmp[key] = payload[key]["S"];
      }
      result['payload'] = tmp;
    }

    let resultMessage = new MessageV2(result, m);
    console.log('Result Message: ' + JSON.stringify(resultMessage));
    return resultMessage;
  }
}

// Service class should
//  * Handle all of the connection & parsing of the data in here
//  ** Handle the data to parse it into the model to work with
//  * Handle the export of message data to outside
//  * Contact eventsController
class WebSocketController {
// TODO XXX: Should change name to WebSocketService instead of Controller
  /*
   * requirements:
   *  background:
   *    persistent: true    // Required to prevent websocket disconnection on page refresh?
   */
  constructor(actionsController) {
    this.host = "wss://nuoooalfmd.execute-api.us-east-1.amazonaws.com/Prod";
    this.websocket = null;
    this.actionsController = actionsController;
    this._connectionStatus = null;

    this.chromeService = new ChromeService();

    this.programInitTime = currentTime();
    this.updateStartTime();
    this.updateIdleTime();
    this.lastDisconnectedTime = null;
  }

  currentTime() {
    return new Date().getTime();
  }

  updateIdleTime() {
    this.idleTime = currentTime();
  }

  updateStartTime() {
    this.startTime = currentTime();
  }

  updateLastDisconnectedTime() {
    this.lastDisconnectedTime = currentTime();
  }

  onmessage(msg) {
      /*
       * requirements:
       *  manifest.json:
       *    permissions: notifications
       */
      WebSocketLogger.preOnmessage(this, msg);


      let message = WebSocketMSGLoaders.message(msg);

      console.log(message.type);
      if(message.type !== 'INSERT') {
        return
      }


      this.updateIdleTime();

      // sendChromeNotification
      var notificationOptions = {
          type: "basic",
          title: message.actionType,
          message: 'Data: ' + JSON.stringify(message.payload),
          iconUrl: "icon.png"
      }
      // TODO XXX: Send message to chrome for processing new action
      chrome.notifications.create("", notificationOptions);

      this.chromeService.sendMessage(message);

      WebSocketLogger.postOnmessage(this, message);
  }

  onopen() {
    WebSocketLogger.preOnopen(this);

    this.updateStartTime();
    this.updateIdleTime();
  }

  onclose() {
    WebSocketLogger.preOnclose(this);

    this.updateLastDisconnectedTime();
    this.websocket = null;

    if(this._connectionStatus === 'connected') {
      console.debug("---- WEBSOCKET RECONNECTING ------");
      this.connect();
    }

    WebSocketLogger.postOnclose(this);
  }

  connect() {
    WebSocketLogger.preConnect(this);

    if('WebSocket' in window && !this.websocket){
      this.websocket = new WebSocket(this.host);
      this._connectionStatus = 'connected';

      this.websocket.onopen    = this.onopen.bind(this);
      this.websocket.onmessage = this.onmessage.bind(this);
      this.websocket.onclose   = this.onclose.bind(this);
    }

    WebSocketLogger.postConnect(this);
  }

  disconnect() {
    if(this.websocket){
      this._connectionStatus = 'disconnected';
      this.websocket.close();
      this.websocket = null;
    }

    WebSocketLogger.postDisconnect(this);
  }
}

export default WebSocketController;
