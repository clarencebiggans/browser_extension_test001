#! /bin/sh
cwd=$(pwd)
echo "${cwd}"
cd ../../popup/
npm run build
cd "${cwd}"
rm main.js
rm index.html
cp ../../popup/dist/main.js main.js
cp ../../popup/dist/index.html index.html
