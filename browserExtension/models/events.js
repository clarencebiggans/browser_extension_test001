
// Wrapper of Event that adds some functionality
class Event {
  constructor(item) {
    this._item = item
  }

  get id() {
    return this._item.id
  }

  get userId() {
    return this._item.user_id
  }

  get type() {
    if(this.actionType === 'updateCurrentURL') {
      return 'state'
    }
    return 'browser'
  }

  get eventName() {
    return this._item.eventName;
  }

  get actionType() {
    return this._item.eventType;
  }

  get payload() {
    return this._item.payload;
  }

  get dateProcessed() {
    return this._item.dateProcessed
  }

  get dateProcessed() {
    return this._item.dateUpdated
  }
}


class EventQueue {
  constructor(events) {
    this._events = events;
  }
  
  dequeue() {
    return this._events.shift();
  }
  
  peek() {
    return this._events[0];
  }

  index(i) {
    return this._events[i];
  }

  get length() {
    return this._events.length;
  }

  forEach(callback) {
    this._events.forEach((event) => callback(event))
  }
}


// NOTE: Would like this to be a const value instead, but can't seem to do that for testing purposes
var EventLoaders = {
    event: function(event) {
        return new Event(event);
    },
    events: function(events) {
        var self = this;

        if(!events || events.length == 0) {
          return new EventQueue([])
        }
        // Wrap all events in their wrapper class
        var resultList = events.map(function(event) {
            return self.event(event);
        })
        // Put into Event Queue
        return new EventQueue(resultList);
    }
}
