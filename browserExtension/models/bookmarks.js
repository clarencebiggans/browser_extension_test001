class Bookmark {
  constructor(item) {
    this._item = item
  }

  get id() {
    return this._item.id;
  }

  get userId() {
    return this._item.user_id;
  }

  get keyId() {
    return this._item.keyId;
  }

  get url() {
    return this._item.url;
  }

  get URL() {
    return this.url;
  }
}

class BookmarkCollection {
  constructor(bookmarks) {
    this._bookmarks = bookmarks
  }

  get first() {
    let bookmark = this._bookmarks[0]
    return bookmark || null;
  }
}


// NOTE: Would like this to be a const value instead, but can't seem to do that for testing purposes
var BookmarkLoaders = {
    bookmark: function(data) {
        return new Bookmark(data);
    },
    bookmarks: function(dataLists) {
        var self = this;

        // Wrap all bookmarks in their wrapper class
        var resultList = dataLists.map(function(data) {
            return self.bookmark(data);
        })

        return new BookmarkCollection(resultList);
    }
}
