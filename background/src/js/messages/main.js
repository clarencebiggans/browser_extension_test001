import WebSocketLogger from "../../js/loggers/websocket.js";
import {ActionV2} from "../../js/models/v2/actions.js";


function parseWebsocketAction(msg) {
  return ActionV2({
    action: msg.action,
    payload: msg.payload,

  }, msg);
};

const MessageLogger = {
  preOnMessage: (request, sendResponse) => {
    console.debug('REQUEST: ' + JSON.stringify(request));
    console.debug('SEND RESPONSE: ' + JSON.stringify(sendResponse));
  }
};


class MessageBus {
  constructor(chromeService, browserController, websocketController, actionsController) {
    this.chromeService = chromeService;
    this.browserController = browserController;
    this.websocketController = websocketController;
    this.actionsController = actionsController;

    this.messageActions = this.messageActions.bind(this);
    this.onPageUpdatedActions = this.onPageUpdatedActions.bind(this);

    this.initializeListeners();
  }

  initializeListeners() {
    this.chromeService.addMessageListener(this.messageActions);
    this.chromeService.addPageUpdateListener(this.onPageUpdatedActions);
  }

  // actions
  messageActions(msg, sender, sendResponse) {
    MessageLogger.preOnMessage(msg, sendResponse);
    // TODO XXX: Should make each action string into a constant so if the action 
    //            doesn't exist, there's an error

    // StateController (Deals with connection state and such)
    if(msg.action === 'activateTab') {
      this.websocketController.connect();
      WebSocketLogger.status(this.websocketController);
    }
    else if(msg.action === 'deactivateTab') {
      WebSocketLogger.status(this.websocketController);
      this.websocketController.disconnect();
    }
    else if(msg.action === 'disconnectWebsocket') {
      console.log('DISCONNECTING WEBSOCKET');
      WebSocketLogger.status(this.websocketController);
      this.websocketController.disconnect();
    }

    // PersistenceController
    else if(msg.action === 'uploadImage') {
      console.log('UPLOADING TO NOTES REPO');
      // TODO XXX: Should create uploadImage action/method in actionsController
      this.actionsController.notesRepo.uploadImage(msg.payload.dataUrl,
                                                  sendResponse);
    }

    // BrowserController
    else if(msg.action === 'takeScreenshot') {
      this.browserController.captureScreenshot((dataUrl) => {
        // Instead of returning sendResponse, should instead dispatch from here
        //  to the persistence controller
        return sendResponse(dataUrl);
      });
    }
    // Should figure out type of action before
    else {
      console.log('Type not found');
      // BrowserController
      this.actionsController.performAction(msg.action);
    }

    return true;
  };

  onPageUpdatedActions(tabId, changeInfo, tab) {
    if (changeInfo.status == 'complete') {
      // Should hash the result and check if it's new or old
      //  Put hash in set and see if it's contained in there or not?
      //  Possibly not, would like to see if go back to a site
      console.debug("######################### PAGE CHANGED #########################")
      console.debug('TabId: ' + tabId);
      console.debug('changeInfo: ' + changeInfo);
      console.debug('tab: ' + JSON.stringify(tab));
      // Wait and attempt for javascript to fully load
      setTimeout(function(){ console.debug('tab: ' + JSON.stringify(tab)); }, 3000);
    }
  };
}

export default MessageBus;
