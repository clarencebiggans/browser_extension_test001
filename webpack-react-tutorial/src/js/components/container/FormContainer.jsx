import React, { Component } from "react";
import Input from "../presentational/Input.jsx";
class FormContainer extends Component {
  constructor() {
    super();
    this.state = {
      seo_title: "",
      result: "Hello there"
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleChange(event) {
    this.setState({ [event.target.id]: event.target.value });
  }
  handleSubmit(event) {
    console.log(event);
    this.setState({ seo_title: "", result: this.state.seo_title });
    event.preventDefault();
  }
  render() {
    const { seo_title, result } = this.state;
    return (
      <div id="TMPID144" style={{display: 'none'}}>
        <form id="article-form" onSubmit={this.handleSubmit}>
          <Input
            text="SEO title"
            label="seo_title"
            type="text"
            id="seo_title"
            value={seo_title}
            handleChange={this.handleChange}
          />
        </form>
        <p>{ result }</p>
      </div>
    );
  }
}
export default FormContainer;
