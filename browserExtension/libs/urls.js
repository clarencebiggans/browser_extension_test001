function formatURL(url) {
  var prefix = url.substring(0, 'https://'.length)
  var prefix2 = url.substring(0, 'http://'.length)
  if(prefix !== 'https://' && prefix2 !== 'http://') {
    url = 'https://' + url
  }

  return url;
}

function urlEquality(url, url_substring) {
  return url.includes(url_substring);
}
