import FormContainer from "./js/components/noteFormApp/containers/FormContainer.jsx";
import React from 'react'
import ReactDOM from 'react-dom'
console.log("================== TOP OF index.jsx FILE =================");


const wrapper = document.createElement("div");
wrapper ? ReactDOM.render(<FormContainer />, wrapper) : false;

wrapper.style.display = "none";

var body = document.getElementsByTagName("BODY")[0];
console.log("INJECTING INTO BODY");
body.appendChild(wrapper);

// ------------------ content.js
function getPlayerDimensions() {
  var player = document.getElementById('player-theater-container');

  return {
    player: player,
    yOffset: player.offsetTop,
    xOffset: player.offsetLeft,
    height: player.offsetHeight,
    width: player.offsetWidth,
  }
}

chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    console.debug('REQUEST: ' + JSON.stringify(request));
    console.debug('TEST IN BROWSER ACTIONS, sender tab: ' + JSON.stringify(sender));
    console.debug('SEND RESPONSE' + JSON.stringify(sendResponse));
    console.debug('Request.action: ');
    
    if(request.state === 'play') {
      document.querySelector("video").play();
    } else if(request.state === 'pause') {
      let video = document.querySelector("video");
      let currentTime = video.currentTime;
      video.currentTime = currentTime - 5;
      video.pause();
    } else if(request.direction === 'back') {
      console.debug('GOING BACK');
      let video = document.querySelector("video");
      let currentTime = video.currentTime;
      console.debug('Current Time: ' + currentTime);
      let rewindTime = request.duration || 10;
      video.currentTime = currentTime - rewindTime;
      console.debug('DONE GOING BACK');
    } else if(request.action === 'captureImage') {
      console.log('Captured Image');
      var img = new Image();
      img.src = request.payload.dataUrl;

      let dimensions = getPlayerDimensions();
      var oCanvas = document.createElement('canvas');

      var oCtx = oCanvas.getContext('2d');
      oCtx.canvas.width = dimensions.width;
      oCtx.canvas.height = dimensions.height;

      img.onload = function() {
        console.log('DIMENSIONS: ' + JSON.stringify(dimensions));

        oCtx.drawImage(img, dimensions.xOffset, dimensions.yOffset,
          dimensions.width, dimensions.height, 0, 0, dimensions.width,
          dimensions.height);

        var body = document.getElementsByTagName("BODY")[0];

        oCanvas.style.position = 'absolute';
        oCanvas.style.top = '6000px';

        body.appendChild(oCanvas);
      }
    } else if(request.action === 'injectBody') {
      console.log('INJECTING BODY');
      injectBody();
      console.log('DONE INJECTING BODY');
      return sendResponse('Response');
      // Next Video:
      // * document.getElementById('related').getElementsByTagName('a')[0]
    }
  }
);

// ------------------------ injectedBody.js
function injectBody() {
  console.log('Injecting');

  var reactEl  = document.getElementById('article-form');
  reactEl.style.display = 'block';

  var liveComments = document.getElementById('columns');

  liveComments.prepend(reactEl);
  console.log('Injected');
};
