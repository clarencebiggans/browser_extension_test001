// class ActionsController {}

class EventsController {
  constructor(eventsRepo, settingsRepo, bookmarksRepo) {
    this.eventsRepo = eventsRepo;
    this.settingsRepo = settingsRepo;
    this.bookmarksRepo = bookmarksRepo;

    this.browserController = new BrowserController();
    // lock?
  }

  async processQueue(userId, callback) {
    let eventQueue = await this.eventsRepo.fetchUnprocessed(userId);

    while(eventQueue.length > 0) {
      let event = eventQueue.dequeue();
      
      let result = await this.processEvent(event);
    }

    callback(eventQueue)
  }

  async processEvent(event, callback) {
    var result;
    // Based on the event, perform action
    console.log('in processEvent: ' + JSON.stringify(event));
    result = await this.performAction(event.userId, event.actionType, event.payload);

    console.log('MARK AS Processed, result: ' + JSON.stringify(result));
    return this.eventsRepo.markProcessed(event.id, event.userId);
  }

  // TODO XXX: should probably just pass in an aciton model object
  async performAction(userId, type, value) {
    var result, browserAction;

    browserAction = {
      type: type,
      payload: undefined
    };

    console.log('PERFORM ACTION, userId: ' + userId);
    // Should probably make the type into verb->noun
      // In this case: 'update' -> 'currentURL'
      // Allows to be more flexible and maintain a better naming convention in future ones
    if(type === 'updateCurrentURL') {
      // get bookmark from bookmarks table
      let bookmark = await this.bookmarksRepo.fetch(userId, value.bookmark);
      console.log('Bookmark: ' + JSON.stringify(bookmark));
      // update remote
      let params = {
        optionValue: bookmark.url
      };
      let optionType = 'currentURL';

      result = await this.settingsRepo.update(userId, optionType, params);

      console.log('PERFORM ACTION, result: ' + JSON.stringify(result));
      console.log('PERFORM ACTION, params: ' + JSON.stringify({
        userId: userId,
        optionType: optionType,
        params: params,
      }));

      browserAction.payload = {url: bookmark.url}
    }

    if(type === 'mediaPlayback') {
      browserAction.payload = value;
    }

    // Perform action on Browser Controller
    this.browserController.performAction(browserAction);

    return result;

    // Look at type
      // See if it's a state change
        //  Update state in Settings table
      //  Perform the action
  }
}
