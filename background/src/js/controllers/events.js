import BrowserController from "./browser.js";

// ---------------------------- controllers/events.js
// class ActionsController {}

class EventsController {
  constructor(browserController, eventsRepo, settingsRepo, bookmarksRepo, notesRepo) {
    this.eventsRepo = eventsRepo;
    this.settingsRepo = settingsRepo;
    this.bookmarksRepo = bookmarksRepo;
    this.notesRepo = notesRepo;

    this.browserController = browserController;
  }

  async processQueue(userId, callback) {
    let eventQueue = await this.eventsRepo.fetchUnprocessed(userId);

    while(eventQueue.length > 0) {
      let event = eventQueue.dequeue();
      
      let result = await this.processEvent(event);
    }

    callback(eventQueue)
  }

  async processEvent(event, callback) {
    var result;
    // Based on the event, perform action
    console.log('in processEvent: ' + JSON.stringify(event));
    result = await this.performAction(event.userId, event.actionType, event.payload);

    console.log('MARK AS Processed, result: ' + JSON.stringify(result));
    return this.eventsRepo.markProcessed(event.id, event.userId);
  }

  async updateCurrentUrl(userId, type, value) {
    var result;

    let bookmark = await this.bookmarksRepo.fetch(userId, value.bookmark);
    console.log('Bookmark: ' + JSON.stringify(bookmark));
    // update remote
    let params = {
      optionValue: bookmark.url
    };
    let optionType = 'currentURL';

    result = await this.settingsRepo.update(userId, optionType, params);

    console.log('PERFORM ACTION, result: ' + JSON.stringify(result));
    console.log('PERFORM ACTION, params: ' + JSON.stringify({
      userId: userId,
      optionType: optionType,
      params: params,
    }));

    return {url: bookmark.url};
  }

  // TODO XXX: should probably just pass in an aciton model object
  // This should receive the message data, then conver it to an event here
  // * Receive it as a message instead of these other values
  // * Want the caller to be able to call methods interchangeably
  // ** Handle parsing the data first thing
  // ** Loose on inputs, strict on outputs
  // * Or do we want the caller to be forced to convert before calling?
  // ** The caller knows exactly what is being called before calling
  // ** The callee has no idea who is calling and with what
  // ** The burden of dependency should be on the caller, not the callee,
  //    because it already has to be dependent on the callee's controller
  // ** It's basically the same thing as calling another service.
  // *** The caller has to form the request into what the callee accepts
  // *** The callee isn't expected to have the burden of forming the info based
  //     on what the caller gives it.
  // async performAction(userId, type, value) {
  async performAction(event) {
    var result, browserAction;

    browserAction = {
      type: type,
      payload: undefined
    };

    console.log('PERFORM ACTION, userId: ' + userId);
    console.log('BROWSER ACTION: ' + JSON.stringify(browserAction));
    // Should probably make the type into verb->noun
      // In this case: 'update' -> 'currentURL'
      // Allows to be more flexible and maintain a better naming convention in future ones
    if(type === 'updateCurrentURL') {
      // get bookmark from bookmarks table
      browserAction.payload = await this.updateCurrentUrl(userId, type, value);
    }

    if(type === 'mediaPlayback') {
      browserAction.payload = value;
    }

    console.log("Browser Action: " + JSON.stringify(browserAction));
    // Perform action on Browser Controller
    this.browserController.performAction(browserAction);

    return result;
  }
}

export default EventsController;
