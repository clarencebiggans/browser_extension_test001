import React, {Component} from 'react'
import Input from "../presentational/Input.jsx";
import {SettingsRepo} from "../../../repos/dist/marks_repo_lib.js";

class FormContainer extends Component {
  constructor() {
    super();
    this.state = {
      seo_title: "",
      notes: ""
    };
    console.log("BEFORE LOADING SETTINGS");
    this.tmp = require("../../../repos/dist/main");
    this.settings_repo = new SettingsRepo();
    this.handleChange = this.handleChange.bind(this);
    this.handleScreenshotClick = this.handleScreenshotClick.bind(this);
  }

  componentDidMount() {
    console.log("BEFORE SETTINGS");
    this.settings_repo.fetch("1", "currentURL", (settings) => {
      console.log("SETTINGS: " + JSON.stringify(settings));
    })
  }

  handleScreenshotClick(event) {
    console.log("Screenshot Clicked");
    // document.getElementById("form-screenshot").addEventListener('click', screenshot);

    // CaptureImage
    // var player = document.getElementById('player-theater-container');
    // var titleContainer = document.getElementById('container');

    //-----------------------------------
    chrome.runtime.sendMessage({
      action: "takeScreenshot",
      payload: "random message"
    }, (response) => {
      console.log('--------RECEIVED RESPONSE: ' + JSON.stringify(response));
      console.log('SENDING uploadImage message');
      chrome.runtime.sendMessage({
         action: "uploadImage",
         payload: {dataUrl: response}
      }, (imageData) => {
        console.log('IMAGE DATA: ' + JSON.stringify(imageData));
      });
    });
  }

  handleChange(event) {
    this.setState({ [event.target.id]: event.target.value });
  }
  render() {
    const { seo_title } = this.state;
    return (
      <div id="article-form">
        <button id="form-screenshot" onClick={this.handleScreenshotClick}>Screenshot</button>
        <form>
          <Input
            text="SEO title"
            label="seo_title"
            type="text"
            id="seo_title"
            value={seo_title}
            handleChange={this.handleChange}
          />
        </form>
      </div>
    );
  }
}
export default FormContainer;
