import BrowserController from "./browser.js";

// ---------------------------- controllers/actions.js
// class ActionsController {}

class ActionsController {
  constructor(browserController, actionsRepo, settingsRepo, bookmarksRepo, notesRepo) {
    this.actionsRepo = actionsRepo;
    this.settingsRepo = settingsRepo;
    this.bookmarksRepo = bookmarksRepo;
    this.notesRepo = notesRepo;

    this.browserController = browserController;
  }

  async updateCurrentUrl(userId, type, value) {
    var result;

    // Get bookmark url
    let bookmark = await this.bookmarksRepo.fetch(userId, value.bookmark);
    console.log('Bookmark: ' + JSON.stringify(bookmark));

    let params = {
      optionValue: bookmark.url
    };
    let optionType = 'currentURL';

    result = await this.settingsRepo.update(userId, optionType, params);

    console.log('PERFORM ACTION, result: ' + JSON.stringify(result));
    console.log('PERFORM ACTION, params: ' + JSON.stringify({
      userId: userId,
      optionType: optionType,
      params: params,
    }));

    return {url: bookmark.url};
  }

  // TODO XXX: should probably just pass in an aciton model object
  // This should receive the message data, then conver it to an action here
  // * Receive it as a message instead of these other values
  // * Want the caller to be able to call methods interchangeably
  // ** Handle parsing the data first thing
  // ** Loose on inputs, strict on outputs
  // * Or do we want the caller to be forced to convert before calling?
  // ** The caller knows exactly what is being called before calling
  // ** The callee has no idea who is calling and with what
  // ** The burden of dependency should be on the caller, not the callee,
  //    because it already has to be dependent on the callee's controller
  // ** It's basically the same thing as calling another service.
  // *** The caller has to form the request into what the callee accepts
  // *** The callee isn't expected to have the burden of forming the info based
  //     on what the caller gives it.
  // async performAction(userId, type, value) {
  async performAction(action) {
    // Should probably make the type into verb->noun
      // In this case: 'update' -> 'currentURL'
      // Allows to be more flexible and maintain a better naming convention in future ones

    // Persistence Actions
    if(action.type === 'updateCurrentURL') {
      // get bookmark from bookmarks table
      let addedKeys = await this.updateCurrentUrl(action.userId, action.type, action.payload);

      action._payload = Object.assign(
        action._payload,
        addedKeys,
      );
    }

    // BrowserActions

    // Perform action on Browser Controller
    this.browserController.performAction(action);

    return result;
  }
}

export default ActionsController;
