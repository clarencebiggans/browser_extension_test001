// ---------------------------- libs/time.js

function currentTime() {
  return new Date().getTime();
}

export {currentTime};
