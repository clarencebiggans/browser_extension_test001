import {uuidv4} from "../../js/libs/uuid.js";
import {formatURL, urlEquality} from "../../js/libs/urls.js";
import {currentTime} from "../../js/libs/time.js";
import WebSocketLogger from "../../js/loggers/websocket.js";
import fetchSettingsAndUpdate from "../../js/controllers/browser.js";
// ---------------------- tabHelpers.js
/*
Many of these functions are functions going between the popup.html and the
backend services. Should separate out or organize these a bit more to
distinguish them from one another. It's all a bit confusing what's happening
here.

This file should:
* Be able to perform actions on/update the tabs
* Get information from the tabs
* This class should talk to a service in the backend code in order to perform
  it's actions
** There needs to be some sort of separation/boundary here, otherwise there's
    just spighetti code
** Should look into communicating through messages/actions from front to back
*** If have a file dedicated to actions, that would be easier to test & get
    right
*** Able to have a messages file for sending commands from front to back

*/


// What dependencies should Chrome Service have?
// * Websocket?
// ** Probably not, should have a controller mediate between websocket and the
//    Browser Commands
class ChromeService {
  // Should take in 
  // TODO XXX: Should have file for messages specifically
  getTab(id, callback) {
    if(!id)
      return callback(null)

    chrome.tabs.get(id, function(tab) {
      if (!!chrome.runtime.lastError) {
        console.warn('WARN: ' + chrome.runtime.lastError.message + ' Possible Reason: Active tab closed');

        return callback(null)
      } else {
        return callback(tab)
      }
    });
  }

  addPageUpdateListener(listenerFunction) {
    chrome.tabs.onUpdated.addListener(listenerFunction);
  }

  addMessageListener(listenerFunction) {
    // chrome.runtime.onMessage.addListener(listenerFunction);
    console.log('ChromeService.addMessageListener');

		console.log('Create onConnect eventListener');
		document.addEventListener("backgroundMessage", (e) => {
			console.log('connected ', e);
			console.log('details ', e.detail);
			listenerFunction(e.detail.msg, undefined, e.detail.callback);
		});

		// Listener for messages from popup/content scripts
		chrome.runtime.onMessage.addListener(listenerFunction);
  }

  sendMessage(msg, callback) {
    console.log("In ChromeService.sendMessage, msg: " + JSON.stringify(msg));

		let testEvent = new CustomEvent("backgroundMessage", {
			detail: {msg: msg, callback: callback}
		});

		document.dispatchEvent(testEvent);
  }

  updateTab(id, params, callback) {
    console.debug('updateTab, id: ' + id + ', params: ' + JSON.stringify(params));

    return chrome.tabs.update(id, params, callback)
  }

  updateTabUrl(id, url, callback) {
    console.debug('updateTabUrl, id: ' + id + ', url: ' + url);

    this.updateTab(id, {url: formatURL(url)}, callback);
  }

  currentActiveTabId(callback) {
    // TODO XXX: Change to loopActiveTabIds
    chrome.storage.local.get(['loopActiveTabs'], function(result) {
      if(!!result.loopActiveTabs && result.loopActiveTabs.length > 0)
        return callback(result.loopActiveTabs[0]);
      return callback(null);
    })
  }

  // TODO XXX: Feel like this should be in another file for page scraping
  // * Would be nice to have scrapers that get different details and performs
  //   different actions on the page
  captureScreenshot(callback) {
    chrome.tabs.captureVisibleTab(null,{format:"jpeg",quality:100},function(dataUrl){
      callback(dataUrl);
    });
  }

  // TODO XXX: Should have messages directory/file to help make the messages
  //           more standard
  // Would like to put this into a better file for organization
  // beginScreenshot() {
  //   this.captureScreenshot((dataUrl) => {
  //     chrome.runtime.sendMessage({
  //       action: 'uploadImage',
  //       payload: {dataUrl: dataUrl}
  //     })
  //   });
  // }

  getActiveTabId(callback) {
    // TODO XXX: Change to loopActiveTabIds
    // TODO XXX: Put the `chrome.storage.local.get` into it's own method for
    //           other uses?
    //           * Not sure if should create a `repo` class for it
    chrome.storage.local.get(['loopActiveTabs'], function(result) {
      if(result.loopActiveTabs.length > 0)
        return callback(result.loopActiveTabs[0])
      return callback(null)
    });
  }

}


function updateTabUrl(tab, url) {
  // Logging of current url and changing to url
  chrome.tabs.update(tab.id, {url: formatURL(url)})
}


function updateCurrentTabUrl(url) {
  chrome.tabs.query({
      active: true,               // Select active tabs
      lastFocusedWindow: true     // In the current window
  }, function(array_of_Tabs) {
      var tab = array_of_Tabs[0];
      updateTabUrl(tab, url)
  });
}

// TODO XXX: This is a frontend function, put in frontend/popup functions
function activateTab(){
  chrome.tabs.query({
      active: true,               // Select active tabs
      lastFocusedWindow: true     // In the current window
  }, function(array_of_Tabs) {
      // createWebSocketConnection();
      // Since there can only be one active tab in one active window, 
      //  the array has only one element
      var tab = array_of_Tabs[0];

      chrome.storage.local.set({loopActiveTabs: [tab.id]})

			// TODO XXX
      // chrome.runtime.sendMessage({
      //   action: 'activateTab',
      //   payload: {tabId: tab.id}
      // });

      // Because this is just called from the popup.js script, the regular logs
      //  don't show
      // fetchSettingsAndUpdate(tab)
      document.getElementById('status').innerHTML = "ACTIVATING"
  });
}


function deactivateTabId(dTabId) {
  chrome.storage.local.get(['loopActiveTabs'], function(result) {
    let resultTabIds = result.loopActiveTabs.filter(function(tabId) {return tabId != dTabId})
    chrome.storage.local.set({loopActiveTabs: resultTabIds}, function() {
      // console.warn('Deactivated: ' + dTabId);
    })
  })
}


function deactivateTab(){
  chrome.tabs.query({
      active: true,               // Select active tabs
      lastFocusedWindow: true     // In the current window
  }, function(array_of_Tabs) {
      // Since there can only be one active tab in one active window, 
      //  the array has only one element
      var tab = array_of_Tabs[0];

      deactivateTabId(tab.id)

			// TODO XXX
      // chrome.runtime.sendMessage({
      //   action: 'deactivateTab',
      //   payload: {tabId: tab.id}
      // });

      document.getElementById('status').innerHTML = "NOT ACTIVE"
  });
}


// ---------------------------- Initializers/init/loaders/env_initilizers/index.jsx/initializeExtension
//  initializers/main.js
//  initilizers/listeners.js

export {ChromeService};
