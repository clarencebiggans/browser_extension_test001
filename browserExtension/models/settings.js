class Setting {
  constructor(item) {
    this._item = item
  }

  get id() {
    return this._item.id;
  }

  get userId() {
    return this._item.userId;
  }

  get optionType() {
    return this._item.optionType;
  }

  get optionValue() {
    return this._item.optionValue;
  }

  get dateUpdated() {
    return this._item.dateUpdated;
  }
}

class SettingCollection {
  constructor(settings) {
    this._settings = settings
  }

  get currentURL() {
    for(const setting of this._settings) {
      if(setting.optionType === 'currentURL') {
        return setting.optionValue;
      }
    }
  }

  get first() {
    if(this._settings.length == 0)
      return null;

    return this._settings[0];
  }
}


// NOTE: Would like this to be a const value instead, but can't seem to do that for testing purposes
var SettingLoaders = {
    setting: function(data) {
        return new Setting(data);
    },
    settings: function(dataLists) {
        var self = this;

        // Wrap all settings in their wrapper class
        var resultList = dataLists.map(function(data) {
            return self.setting(data);
        })

        return new SettingCollection(resultList);
    }
}
