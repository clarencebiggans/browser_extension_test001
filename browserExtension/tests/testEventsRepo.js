var fs = require('fs');
var fetch = require('node-fetch');

// file is included here:
eval(fs.readFileSync('./models/events.js')+'');
var BaseRepo = eval('(' + fs.readFileSync('./repos/baseRepo.js')  +')');
var EventsRepo = eval('(' + fs.readFileSync('./repos/eventsRepo.js')  +')');
var eventRepo = new EventsRepo();
var testUserId = '0';

const testEventsRepo_fetchUnprocessed = () => {

  eventRepo.fetchUnprocessed(testUserId, (events) => {
    console.log(events);
    console.log(events.peek());
    console.log(events._events.length);
  });
}

const testEventsRepo_add = () => {
  let params = {
    event: {
      userId: testUserId,
      type: 'test-updateCurrentURL',
      payload: {bookmark: 'meditation'},
    }
  };

  eventRepo.add(params, (result) => {
    console.log(result);
    console.log(!!result);
  });
}

const testEventsRepo_markProcessed = () => {
  let params = {
    event: {
      userId: testUserId,
      type: 'test-updateCurrentURL',
      payload: {bookmark: 'meditation'},
    }
  };

  eventRepo.add(params, (event) => {
    eventRepo.markProcessed(event.id, event.userId, (result) => {
      console.log(result);
    });
  });
}

testEventsRepo_fetchUnprocessed()
testEventsRepo_add()
testEventsRepo_markProcessed()
