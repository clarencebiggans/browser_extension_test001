function currentTime() {
  return new Date().getTime();
}
var WebSocketLogger = {
  status: (websocketController) => {
    console.log('-------------------------------------');
    console.log('Websocket Program Init Duration: ' + ((currentTime() - websocketController.programInitTime)/1000) + ' seconds');
    console.log('Websocket Open Duration: ' + ((currentTime() - websocketController.startTime)/1000) + ' seconds');
    console.log('Websocket idle time Duration: ' + ((currentTime() - websocketController.idleTime)/1000) + ' seconds');
    console.log('Websocket _connectionStatus: ' + websocketController._connectionStatus);
  },

  preOnmessage: (websocketController, msg) => {
    console.debug('MESSAGE RECEIVED: ' + msg.data);
  },

  postOnmessage: (websocketController, event) => {
    console.debug('EVENT: ' + JSON.stringify(event));
  },

  preOnopen: (websocketController) => {
    console.debug('WEBSOCKET OPENED');
    if(!!websocketController.lastDisconnectedTime) {
      let duration = (currentTime() - websocketController.lastDisconnectedTime)/1000;
      console.debug("RECONNECTION DURATION: " + duration + " seconds");
    }
  },

  preOnclose: (websocketController) => {
    console.debug("==== Closing WebSocket ======");
    console.debug("WEBSOCKET TIME DURATION: " + (currentTime() - websocketController.startTime));
    console.debug("WEBSOCKET IDLE TIME DURATION: " + (currentTime() - websocketController.idleTime));
  },

  postOnclose: (websocketController) => {
    console.debug("==== WebSocket Closed ======");
  },

  preConnect: (websocketController) => {
    console.debug("Current Time: " + new Date(currentTime()).toString());
    console.debug('WEBSOCKET: ' + JSON.stringify(websocketController.websocket));
  },

  postConnect: (websocketController) => {
    console.debug("======== websocket ===========",
                  websocketController.websocket);
  },

	postDisconnect: (websocketController) => {
    console.debug("======== disconnecting websocket ===========",
                  websocketController.websocket);
	}
}
