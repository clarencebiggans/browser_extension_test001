var fs = require('fs');
var fetch = require('node-fetch');

// file is included here:
eval(fs.readFileSync('./models/settings.js')+'');
var BaseRepo = eval('(' + fs.readFileSync('./repos/baseRepo.js')  +')');
var SettingsRepo = eval('(' + fs.readFileSync('./repos/settingsRepo.js')  +')');

var settingsRepo = new SettingsRepo();
var testUserId = '0';

function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}

const testSettingsRepo_fetch = (userId) => {
  settingsRepo.fetch(userId, null, (settings) => {
    console.log(settings);
    console.log(settings._settings[0]._item);
  });
}

const testSettingsRepo_update = () => {
  let params = {
    optionValue: 'www.example7.com',
  };
  let userId = testUserId;
  let optionType = 'currentURL';

  settingsRepo.update(userId, optionType, params, (result) => {
    console.log(result);
    console.log(result.id);
    console.log(!!result);
  });
}

const testSettingsRepo_create = () => {
  let userId = testUserId;
  let optionType = 'test-currentURL-' + uuidv4();
  let optionValue= 'www.example7.com';

  settingsRepo.create(userId, optionType, optionValue, (result) => {
    console.log(result);
    console.log(result.id);
    console.log(!!result);

    // Test that on second call with same userId/optionType returns back null
    settingsRepo.create(userId, optionType, optionValue, (result) => {
      console.log(result == null);
    });
  });
}

// testSettingsRepo_fetch('1')
// testSettingsRepo_update()
testSettingsRepo_create()
