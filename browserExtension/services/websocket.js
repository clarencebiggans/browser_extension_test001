/*
NOTE:
* Anything that accesses the Browser/Outside services should be put
  in Services Folder, except repositories that access database-like strucutres
*/

class WebSocketMSGLoaders {
  static event(event) {
    var result = {};

    let e = JSON.parse(event.data);
    result['eventName'] = e.eventName;

    let i = e.dynamodb.NewImage;
    if(i.user_id) {
      result['userId'] = i.user_id['S'];
      result['user_id'] = i.user_id['S'];
    }
    if(i.id) {
      result['id'] = i.id['S'];
    }
    if(i.eventType) {
      result['eventType'] = i.eventType['S'];
    }
    if(i.payload) {
      let payload = i.payload['M'];
      var tmp = {};

      for(var key in payload) {
        tmp[key] = payload[key]["S"];
      }
      result['payload'] = tmp;
    }

    return new marks_repo_lib.Event(result);
  }
}

// Service class should
//  * Handle all of the connection & parsing of the data in here
//  ** Handle the data to parse it into the model to work with
//  * Handle the export of message data to outside
//  * Contact eventsController
class WebSocketController {
  /*
   * requirements:
   *  background:
   *    persistent: true    // Required to prevent websocket disconnection on page refresh?
   */
  constructor(eventsController) {
    this.host = "wss://nuoooalfmd.execute-api.us-east-1.amazonaws.com/Prod";
    this.websocket = null;
    this.eventsController = eventsController;
    this._connectionStatus = null;

    this.programInitTime = currentTime();
    this.updateStartTime();
    this.updateIdleTime();
    this.lastDisconnectedTime = null;
  }

  currentTime() {
    return new Date().getTime();
  }

  updateIdleTime() {
    this.idleTime = currentTime();
  }

  updateStartTime() {
    this.startTime = currentTime();
  }

  updateLastDisconnectedTime() {
    this.lastDisconnectedTime = currentTime();
  }

  onmessage(msg) {
      /*
       * requirements:
       *  manifest.json:
       *    permissions: notifications
       */
      WebSocketLogger.preOnmessage(this, msg);

      this.updateIdleTime();
      var event = WebSocketMSGLoaders.event(msg);

      // sendChromeNotification
      var notificationOptions = {
          type: "basic",
          title: event.actionType,
          message: 'Data: ' + JSON.stringify(event.payload),
          iconUrl: "icon.png"
      }
      chrome.notifications.create("", notificationOptions);

      if(event.eventName === 'INSERT') {
        this.eventsController.processEvent(event);
      }

      WebSocketLogger.postOnmessage(this, event);
  }

  onopen() {
    WebSocketLogger.preOnopen(this);

    this.updateStartTime();
    this.updateIdleTime();
  }

  onclose() {
    WebSocketLogger.preOnclose(this);

    this.updateLastDisconnectedTime();
    this.websocket = null;

    if(this._connectionStatus === 'connected') {
      console.debug("---- WEBSOCKET RECONNECTING ------");
      this.connect();
    }

    WebSocketLogger.postOnclose(this);
  }

  connect() {
    WebSocketLogger.preConnect(this);

    if('WebSocket' in window && !this.websocket){
      this.websocket = new WebSocket(this.host);
      this._connectionStatus = 'connected';

      this.websocket.onopen    = this.onopen.bind(this);
      this.websocket.onmessage = this.onmessage.bind(this);
      this.websocket.onclose   = this.onclose.bind(this);
    }

    WebSocketLogger.postConnect(this);
  }

  disconnect() {
    if(this.websocket){
      this._connectionStatus = 'disconnected';
      this.websocket.close();
      this.websocket = null;
    }

    WebSocketLogger.postDisconnect(this);
  }
}
