async function fetchSettingsAndUpdate(tab) {
  var userId = '1';

  var eventsController = new EventsController(
    eventsRepo, settingsRepo, bookmarksRepo
  )

  await eventsController.processQueue(userId, (result) => {
    console.log(result);
  });

  var settings = await settingsRepo.fetch(userId);
  var tabUrl = tab.url.trim();
  var remoteCurrentUrl = settings.currentURL.trim();

  if(!urlEquality(tabUrl, remoteCurrentUrl)) {
    console.log('CHANGING TO REMOTE CURRENT URL')
    updateTabUrl(tab, remoteCurrentUrl)
  } else {
    console.log('ON CORRECT CURRENT URL, NOT REDIRECTING')
  }
}
