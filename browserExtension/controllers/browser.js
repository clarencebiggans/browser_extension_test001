class BrowserController {
  async activeTabId() {
    return new Promise(resolve => {
      // TODO XXX: Change to loopActiveTabIds
      chrome.storage.local.get(['loopActiveTabs'], function(result) {
        if(result.loopActiveTabs.length > 0)
          return resolve(result.loopActiveTabs[0])
        return resolve(null)
      })
    })
  }

  async getTab(id) {
    if(!id)
      return null
    return new Promise(resolve => {
      chrome.tabs.get(id, function(tab) {
        if (chrome.runtime.lastError) {
          console.log('WARN: ' + chrome.runtime.lastError.message + ' Possible Reason: Active tab closed');
          return resolve(null)
        } else {
          return resolve(tab)
        }
      })
    })
  }

  updateTabUrl(tab, url) {
    // Logging of current url and changing to url
    console.debug('updateTabUrl, tab: ' + JSON.stringify(tab) + ', url: ' + JSON.stringify(url))
    chrome.tabs.update(tab.id, {url: formatURL(url)})
  }

  async performAction(action, callback) {
    var activeTabId = await this.activeTabId();
    var activeTab = await this.getTab(activeTabId);

    if(action.type === 'updateCurrentURL') {
      var tabUrl = activeTab.url.trim();
      var remoteCurrentUrl = action.payload.url.trim();

      if(!urlEquality(tabUrl, remoteCurrentUrl)) {
        console.log('CHANGING TO REMOTE CURRENT URL')
        this.updateTabUrl(activeTab, remoteCurrentUrl)
      } else {
        console.log('ON CORRECT CURRENT URL, NOT REDIRECTING')
      }
    }

    if(action.type === 'mediaPlayback') {
      console.debug('ACTIVE TAB: ' + JSON.stringify(activeTab));
      chrome.tabs.sendMessage(activeTabId, action.payload);
    }
  }
}
