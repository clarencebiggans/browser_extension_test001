import React, { Component } from "react";
import ReactDOM from "react-dom";
import FormContainer from "./js/components/container/FormContainer.jsx";

// This will run after DOM is loaded per the `run_at` property in the manifest.json
//  It's currently set at 'document_idle', which means this script will run between
//  `document_end` and immediately after the `window.onload` event fires.
//  https://developer.chrome.com/extensions/content_scripts
console.log('Page has loaded, adding react components to body');
const wrapper = document.createElement('div');
wrapper ? ReactDOM.render(<FormContainer />, wrapper) : false;
const body = document.getElementsByTagName("BODY")[0];
body.appendChild(wrapper);


// CHANGES NOTES

// Add wrapper code to body of webpage
//  Should only perform this action once the webpage loads or at least listen
//  to when body element has been added to the page
//  Might also be better to put this in index.js instead of here
//    index.js handles how this gets treated by others importing in this
//    component and the presentation of it. Also, the display style should also
//    be set there since people using this form might want to customize that
//    and use this differently. While setting the display style to `none` is
//    done in our case, we might want to reuse this code in other places where
//    that may not be done
