var fs = require('fs');
var fetch = require('node-fetch');

// file is included here:
eval(fs.readFileSync('./models/bookmarks.js')+'');
var BaseRepo = eval('(' + fs.readFileSync('./repos/baseRepo.js')  +')');
var BookmarksRepo = eval('(' + fs.readFileSync('./repos/bookmarksRepo.js')  +')');

var bookmarksRepo = new BookmarksRepo();

var testUserId = '0';


function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}


const testBookmarksRepo_fetch = () => {
  bookmarksRepo.fetch(testUserId, 'meditation', (bookmark) => {
    console.log(bookmark);
    console.log(bookmark._item);
  });
  bookmarksRepo.fetch(testUserId, 'google', (bookmark) => {
    console.log(bookmark);
    console.log(bookmark._item);
  });
}

const testBookmarksRepo_create = () => {
  var userId = testUserId;
  var keyId = 'testB2-' + uuidv4();
  var url = 'www.exampleTestB1.com';

  bookmarksRepo.create(userId, keyId, url, (bookmark) => {
    console.log(bookmark);
    console.log(bookmark._item);

    bookmarksRepo.create(userId, keyId, url, (bookmark) => {
      console.log(bookmark);
      console.log(bookmark == null);
    });
  });
}

/*
const seedData = () => {
  var userId = '0'
  var keyId = 'meditation'
  var url = 'https://www.youtube.com/watch?v=PuyRsGtNnmw'

  bookmarksRepo.create(userId, keyId, url, (bookmark) => {
    console.log(bookmark);
    console.log(bookmark._item);
  });

  var keyId = 'google'
  var url = 'https://www.google.com/'

  bookmarksRepo.create(userId, keyId, url, (bookmark) => {
    console.log(bookmark);
    console.log(bookmark._item);
  });
}
*/

// seedData();

testBookmarksRepo_fetch();
// testBookmarksRepo_create();
