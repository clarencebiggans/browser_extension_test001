const BackgroundScriptLogger = {
  preOnMessage: (request, sender, sendResponse) => {
    console.debug('REQUEST: ' + JSON.stringify(request));
    console.debug('sender tab: ' + JSON.stringify(sender));
    console.debug('SEND RESPONSE: ' + JSON.stringify(sendResponse));
  }
};

var eventsRepo = new marks_repo_lib.EventsRepo();
var settingsRepo = new marks_repo_lib.SettingsRepo();
var bookmarksRepo = new marks_repo_lib.BookmarksRepo();
var notesRepo = new marks_repo_lib.NotesRepo();
var eventsController = new EventsController(
  eventsRepo, settingsRepo, bookmarksRepo);

window.websocketController = new WebSocketController(eventsController);

// Put this into another file dedicated to messages
chrome.runtime.onMessage.addListener(
  // async function(request, sender, sendResponse) {
  function(request, sender, sendResponse) {
    console.log('IN BACKGROUNDSCRIPT.JS');
    // BackgroundScriptLogger.preOnMessage(request, sender, sendResponse);

    if(request.action === 'activateTab') {
      window.websocketController.connect();
      WebSocketLogger.status(window.websocketController);
    }

    if(request.action === 'deactivateTab') {
      WebSocketLogger.status(window.websocketController);
      window.websocketController.disconnect();
    }

    // if(request.action === 'captureImage') {
    //   console.log('IN CAPTURE_IMAGE ACTION IN BACKGROUNDSCRIPT.js');
    //   var activeTabId = await currentActiveTabId(sendResponse);
    //   chrome.tabs.sendMessage(activeTabId, request);

    //   console.log('UPLOADING TO NOTES REPO - UPLOAD_IMAGE');
    //   notesRepo.uploadImage(request.payload.dataUrl);
    // }

    if(request.action === 'uploadImage') {
      console.log('IN UPLOAD_IMAGE ACTION IN BACKGROUNDSCRIPT.js');

      console.log('UPLOADING TO NOTES REPO - UPLOAD_IMAGE, dataUrl: ' + request.payload.dataUrl);
      notesRepo.uploadImage(request.payload.dataUrl, sendResponse);
    }

    return true;
  }
);


window.onload = async () => {
  console.debug('Loaded BackgroundScript');
  var curActiveTabId = await currentActiveTabId();
  
  if(!!curActiveTabId) {
    var curActiveTab = await getTab(curActiveTabId);

    if(!!curActiveTab) {
      window.websocketController.connect();
      WebSocketLogger.status(window.websocketController);
    }
  }
};

chrome.tabs.onUpdated.addListener( function (tabId, changeInfo, tab) {
  if (changeInfo.status == 'complete') {
		console.debug("######################### PAGE CHANGED #########################")
		console.debug('TabId: ' + tabId);
		console.debug('changeInfo: ' + changeInfo);
		console.debug('tab: ' + JSON.stringify(tab));
    // Wait and attempt for javascript to load fully
    setTimeout(function(){ console.debug('tab: ' + JSON.stringify(tab)); }, 3000);
  }
})
