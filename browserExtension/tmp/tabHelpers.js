// ---------------------- tabHelpers.js
/*
Many of these functions are functions going between the popup.html and the
backend services. Should separate out or organize these a bit more to
distinguish them from one another. It's all a bit confusing what's happening
here.

This file should:
* Be able to perform actions on/update the tabs
* Get information from the tabs
* This class should talk to a service in the backend code in order to perform
  it's actions
** There needs to be some sort of separation/boundary here, otherwise there's
    just spighetti code
** Should look into communicating through messages/actions from front to back
*** If have a file dedicated to actions, that would be easier to test & get
    right
*** Able to have a messages file for sending commands from front to back

*/

async function getTab(id) {
  if(!id)
    return null
  return new Promise(resolve => {
    chrome.tabs.get(id, function(tab) {
      if (chrome.runtime.lastError) {
        console.log('WARN: ' + chrome.runtime.lastError.message + ' Possible Reason: Active tab closed');
        
        // TODO XXX: REPLICATED CODE
        if(window.websocketController && window.websocketController.websocket) {
          WebSocketLogger.status(window.websocketController);
          window.websocketController.disconnect();
        }
        return resolve(null)
      } else {
        return resolve(tab)
      }
    })
  })
}


function updateTabUrl(tab, url) {
  // Logging of current url and changing to url
  chrome.tabs.update(tab.id, {url: formatURL(url)})
}


function updateCurrentTabUrl(url) {
  chrome.tabs.query({
      active: true,               // Select active tabs
      lastFocusedWindow: true     // In the current window
  }, function(array_of_Tabs) {
      var tab = array_of_Tabs[0];
      updateTabUrl(tab, url)
  });
}


// TODO XXX: This is a frontend function, put in frontend/popup functions
function activateTab(){
  chrome.tabs.query({
      active: true,               // Select active tabs
      lastFocusedWindow: true     // In the current window
  }, function(array_of_Tabs) {
      // createWebSocketConnection();
      // Since there can only be one active tab in one active window, 
      //  the array has only one element
      var tab = array_of_Tabs[0];

      chrome.storage.local.set({loopActiveTabs: [tab.id]})
      chrome.runtime.sendMessage({
        action: 'activateTab',
        payload: {tabId: tab.id}
      });

      // Because this is just called from the popup.js script, the regular logs
      //  don't show
      // fetchSettingsAndUpdate(tab)
      document.getElementById('status').innerHTML = "ACTIVATING"
  });
}


function deactivateTabId(dTabId) {
  chrome.storage.local.get(['loopActiveTabs'], function(result) {
    let resultTabIds = result.loopActiveTabs.filter(function(tabId) {return tabId != dTabId})
    chrome.storage.local.set({loopActiveTabs: resultTabIds}, function() {
      // console.warn('Deactivated: ' + dTabId);
    })
  })
}


function deactivateTab(){
  chrome.tabs.query({
      active: true,               // Select active tabs
      lastFocusedWindow: true     // In the current window
  }, function(array_of_Tabs) {
      // Since there can only be one active tab in one active window, 
      //  the array has only one element
      var tab = array_of_Tabs[0];

      deactivateTabId(tab.id)
      chrome.runtime.sendMessage({
        action: 'deactivateTab',
        payload: {tabId: tab.id}
      });

      document.getElementById('status').innerHTML = "NOT ACTIVE"
  });
}


async function currentActiveTabId() {
  return new Promise(resolve => {
    // TODO XXX: Change to loopActiveTabIds
    chrome.storage.local.get(['loopActiveTabs'], function(result) {
      if(!!result.loopActiveTabs && result.loopActiveTabs.length > 0)
        return resolve(result.loopActiveTabs[0])
      return resolve(null)
    })
  })
}

// TODO XXX: Feel like this should be in another file for page scraping
// * Would be nice to have scrapers that get different details and performs
//   different actions on the page
function captureScreenshot(callback) {
  chrome.tabs.captureVisibleTab(null,{format:"jpeg",quality:100},function(dataUrl){
    callback(dataUrl);
  });
}

// Would like to put this into a better file for oranization
function beginScreenshot() {
  captureScreenshot((dataUrl) => {
    chrome.runtime.sendMessage({
      action: 'uploadImage',
      payload: {dataUrl: dataUrl}
    })
  });
}

chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    console.log("RECEIVED MESSAGE: " + JSON.stringify(request));

    if(request.action === 'takeScreenshot') {
      captureScreenshot((dataUrl) => {
        return sendResponse(dataUrl);
      });
      return true;
    }
  }
);

