import WebSocketLogger from "../../js/loggers/websocket.js";
import {ChromeService} from "../../js/services/chrome.js";
import "../../js/repos/marks_repo_lib.js"; // marks_repo_lib
import BrowserController from "../../js/controllers/browser.js";
import ActionsController from "../../js/controllers/actions.js";
import WebSocketController from "../../js/services/websocket.js";
import MessageBus from "../../js/messages/main.js";

// ---------------------------- backgroundScript.js


window.actionsRepo = new marks_repo_lib.ActionsRepo();
window.settingsRepo = new marks_repo_lib.SettingsRepo();
window.bookmarksRepo = new marks_repo_lib.BookmarksRepo();
window.notesRepo = new marks_repo_lib.NotesRepo();
window.chromeService = new ChromeService();
window.browserController = new BrowserController(chromeService);

window.actionsController = new ActionsController(
      window.browserController, window.actionsRepo, window.settingsRepo, window.bookmarksRepo, window.notesRepo);

window.websocketController = new WebSocketController(window.actionsController);

window.messageBus = new MessageBus(window.chromeService,
                                   window.browserController,
                                   window.websocketController,
                                   window.actionsController);

window.onload = async () => {
  console.debug('Loaded BackgroundScript');
  var curActiveTabId = await window.browserController.currentActivatedTabId();
  
  if(!!curActiveTabId) {
    var curActiveTab = await window.browserController.getTab(curActiveTabId);

    if(!!curActiveTab) {
      window.messageBus.websocketController.connect();
      WebSocketLogger.status(window.messageBus.websocketController);
    }
  }
};
