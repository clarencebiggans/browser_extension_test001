import FormContainer from "./js/components/container/FormContainer.jsx";

const wrapper = document.getElementById("create-article-form");
wrapper ? ReactDOM.render(<FormContainer />, wrapper) : false;


// =========================================================================
// TODO XXX: Shouldn't access chrome.tabs directly from the popup code, should
//            send messages to the BackgroundScript instead.
// =========================================================================

// ---------------------- tabHelpers.js

// TODO XXX: This is a frontend function, put in frontend/popup functions
function activateTab(){
  chrome.tabs.query({
      active: true,               // Select active tabs
      lastFocusedWindow: true     // In the current window
  }, function(array_of_Tabs) {
      // createWebSocketConnection();
      // Since there can only be one active tab in one active window, 
      //  the array has only one element
      var tab = array_of_Tabs[0];

      chrome.storage.local.set({loopActiveTabs: [tab.id]})
      chrome.runtime.sendMessage({
        action: 'activateTab',
        payload: {tabId: tab.id}
      });

      // Because this is just called from the popup.js script, the regular logs
      //  don't show
      // fetchSettingsAndUpdate(tab)
      document.getElementById('status').innerHTML = "ACTIVATING"
  });
}


function deactivateTabId(dTabId) {
  chrome.storage.local.get(['loopActiveTabs'], function(result) {
    let resultTabIds = result.loopActiveTabs.filter(function(tabId) {return tabId != dTabId})
    chrome.storage.local.set({loopActiveTabs: resultTabIds}, function() {
      // console.warn('Deactivated: ' + dTabId);
    })
  })
}


function deactivateTab(){
  chrome.tabs.query({
      active: true,               // Select active tabs
      lastFocusedWindow: true     // In the current window
  }, function(array_of_Tabs) {
      // Since there can only be one active tab in one active window, 
      //  the array has only one element
      var tab = array_of_Tabs[0];

      deactivateTabId(tab.id)
      chrome.runtime.sendMessage({
        action: 'deactivateTab',
        payload: {tabId: tab.id}
      });

      document.getElementById('status').innerHTML = "NOT ACTIVE"
  });
}


async function currentActiveTabId() {
  return new Promise(resolve => {
    // TODO XXX: Change to loopActiveTabIds
    chrome.storage.local.get(['loopActiveTabs'], function(result) {
      if(!!result.loopActiveTabs && result.loopActiveTabs.length > 0)
        return resolve(result.loopActiveTabs[0])
      return resolve(null)
    })
  })
}

// TODO XXX: Feel like this should be in another file for page scraping
// * Would be nice to have scrapers that get different details and performs
//   different actions on the page
function captureScreenshot(callback) {
  chrome.tabs.captureVisibleTab(null,{format:"jpeg",quality:100},function(dataUrl){
    callback(dataUrl);
  });
}

// Would like to put this into a better file for oranization
function beginScreenshot() {
  captureScreenshot((dataUrl) => {
    chrome.runtime.sendMessage({
      action: 'uploadImage',
      payload: {dataUrl: dataUrl}
    })
  });
}

chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    console.log("RECEIVED MESSAGE: " + JSON.stringify(request));

    if(request.action === 'takeScreenshot') {
      captureScreenshot((dataUrl) => {
        return sendResponse(dataUrl);
      });
      return true;
    }
  }
);

// ---------------------- listeners.js
// document.getElementById('activate').addEventListener('click', fetchSettingsAndUpdate);
document.getElementById('activate').addEventListener('click', activateTab);
document.getElementById('deactivate').addEventListener('click', deactivateTab);
document.getElementById('screenshot').addEventListener('click', beginScreenshot);
document.getElementById('inject').addEventListener('click', () => {
  chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    chrome.tabs.sendMessage(tabs[0].id, {action: 'injectBody'}, function(response) {
      console.log(response);
    });
  });
});

// ---------------------- popup.js
$(function() {
  chrome.tabs.query({active: true, lastFocusedWindow: true}, function(array_of_tabs) {
    var currentTab = array_of_tabs[0];
    chrome.storage.local.get(['loopActiveTabs'], function(result) {
      if(!result.loopActiveTabs)
        result.loopActiveTabs = [];

      var i;
      for (i = 0; i < result.loopActiveTabs.length; i++) {
        if (result.loopActiveTabs[i] == currentTab.id) {
          return document.getElementById('status').innerHTML = "ACTIVATED";
        }
      }
      return document.getElementById('status').innerHTML = "DEACTIVATED";
    })
  })
});
